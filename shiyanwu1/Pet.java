package shiyanwu1;


public interface Pet {
	public abstract String getNum();
	public abstract String getKind();
	public abstract double getPrice();
	public abstract int getCount();
	public abstract void setCount(int count);
}