package shiyansi4;


import java.util.Scanner;

/*abstract class Animal{
	public abstract void eat();
}*/

class Lion{
	public Lion(){
		
	}
	public void eat(){
		System.out.println("狮子被喂食了。");
	}
}

class Monkey{
	private int num;
	public Monkey(){
		
	}
	public Monkey(int num){
		this.num=num;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public void eat(){
		System.out.println(num+"号猴子被喂食了。");
	}
}


class Pigeon{
	private int num;
	public Pigeon(){
		
	}
	public Pigeon(int num){
		this.num=num;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public void eat(){
		System.out.println(num+"号鸽子被喂食了。");
	}
}

class Feeder{
	private String name;
	public Feeder(){
		
	}
	public Feeder(String name){
		this.name=name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void FeedLion(Lion l){
		l.eat();
	}
	public void FeedMonkey(Monkey m){
		m.eat();
	}
	public void FeedPigeon(Pigeon p){
		p.eat();
	}
}



public class shiyansi44 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner n=new Scanner(System.in);
		System.out.println("饲养员的姓名");
		String name=n.nextLine();
		Feeder f1=new Feeder(name);
		Lion l1=new Lion();
		f1.FeedLion(l1);
		Monkey m[]=new Monkey[5];
		for(int i=0;i<5;i++){
			m[i]=new Monkey(i+1);
			f1.FeedMonkey(m[i]);
		}
		Pigeon p[]=new Pigeon[10];
		for(int j=0;j<10;j++){
			p[j]=new Pigeon(j+1);
			f1.FeedPigeon(p[j]);
		}
    
	}

}