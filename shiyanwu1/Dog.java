package shiyanwu1;


public class Dog implements Pet {
	private String num,kind;
	private static int n;
	private int count;
	private double price;
	static{
		setN(0);
	}
	public Dog(){
		
	}
	public Dog(String kind,double price,int n){
		this.num="100"+(++n);
		this.kind=kind;
		this.price=price;
		this.count=n;
	}
	public Dog(Dog x){
		this.num=x.getNum();
		this.kind=x.getKind();
		this.price=x.getPrice();
		this.count=x.getCount();
	}
	public  void setCount(int count){
		this.count = count;
	}
	public String getNum(){
		return num;
	}
	public String getKind(){
		return kind;
	}
	public double getPrice(){
		return price;
	}
	public  int getCount(){
		return count;
	}
	public static int getN() {
		return n;
	}
	public static void setN(int n) {
		Dog.n = n;
	}
}