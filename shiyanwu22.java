package shiyanwu2;
interface Animal{
    abstract String cry();
    abstract String getAnimaName();
}

class cat implements Animal{
    public String cry() {
        return "������";
    }
    public String getAnimaName() {
        return "è";
    }
}

class dog implements Animal{
    public String cry() {
        return "������";
    }
    public String getAnimaName() {
        return "��";
    }
}

class Simulator{
    void playSound(Animal animal) {
        System.out.println(animal.cry());
        System.out.println(animal.getAnimaName());
    }
}

public class shiyanwu22 {
    public static void main(String[] args) {
        Simulator simulator = new Simulator();
        simulator.playSound(new dog());
        simulator.playSound(new cat());
    }

}