package shiyanwu1;



import java.util.Scanner;

public class Shop {
	private Pet[] pets,buyPets;
	private int foot;
	Scanner in=new Scanner(System.in);
	public Shop(){
		
	}
	public Shop(Pet[] pets){
		this.pets=pets;
		this.foot=pets.length;
	}
	public void display(){
		@SuppressWarnings("unused")
		int n=0;
		for(int i=0;i<pets.length;i++){
			if(pets[i] instanceof Cat){
				System.out.print("种类：猫");
			}
			else if(pets[i] instanceof Dog){
				System.out.print("种类：狗");
			}
			System.out.print("  编号："+pets[i].getNum());
			System.out.println(" 品种："+pets[i].getKind()+" 单价："+pets[i].getPrice()+"数量："+pets[i].getCount());
		}
	}
	public Pet[] buyPet(){
		int n,i,count;
		String key;
		System.out.println("请输入购买宠物种类个数：");
	    	n=in.nextInt();
	    if(n>foot)
	    	n=foot;
	    this.buyPets=new Pet[n];
		a:for( int j=0;j<n;j++){
			System.out.println("请输入购买品种：");
			key=in.next();
			for( i=0;i<pets.length;i++){
				if(pets[i].getKind().equals(key)){
					if(pets[i].getCount()==0){
						System.out.println("sorry，亲爱的顾客，本商店该类宠物已售罄，请重新选择。");
						j--;
						continue a;
					}
					else{
						System.out.println(pets[i].getKind()+"有"+pets[i].getCount()+"只,请输入购买数量。");
						count=in.nextInt();
						if(count>=pets[i].getCount()){
							count=pets[i].getCount();
							this.foot--;
						}
						if(pets[i] instanceof Cat){
							Cat temp=(Cat)pets[i];
							buyPets[i]=new Cat(temp);
						}
						else if(pets[i] instanceof Dog){
							Dog temp=(Dog)pets[i];
							buyPets[j]=new Dog(temp);
						}
						if(buyPets[j]!=null)
							buyPets[j].setCount(count);
						pets[i].setCount(pets[i].getCount()-count);
						System.out.println("购买成功，"+count+"只,"+pets[i].getKind());	
						continue a;
					}
				}
			}
			if(i==pets.length){
				System.out.println("sorry,本商店没有此类宠物，请重新选择。");
				j--;
				continue;
			}
		}
		return buyPets;
	}
	public void displayBuy(){
		int n=0;
		double p=0;
		if(buyPets==null)
			return ;
		for(int i=0;i<buyPets.length;i++){
			if(buyPets[i] instanceof Cat){
				System.out.print("种类：猫");
			}
			else if(buyPets[i] instanceof Dog){
				System.out.print("种类：狗");
			}
			n+=buyPets[i].getCount();
			p+=buyPets[i].getPrice();
			System.out.print(" 编号："+buyPets[i].getNum()+"品种："+buyPets[i].getKind()+" 单价："+buyPets[i].getPrice()+"数量："+buyPets[i].getCount());
			System.out.println("花费:"+String.format("%.2f元", buyPets[i].getCount()*buyPets[i].getPrice()));
		}
		System.out.println("总计购买宠物"+n+"只，花费"+String.format("%.2f元",p));
	}
}