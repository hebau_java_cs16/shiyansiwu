package shiyansi3;

import java.util.Scanner;

abstract class ping{//周长、面积 
	public abstract double mian();//矩形面积
	public abstract double zhou();//矩形周长
}
abstract class li{//表面积、体积
	public abstract double biao(); //表面积
	public abstract double ti();//体积
}
class qiu extends li{ //球类
	double r;
	qiu(double r){
		this.r=r;
	}
	public double biao(){ //表面积
		return (4*3.14*r*r);
	}
	public double ti(){
		return((double)4/3*3.14*r*r*r);
	}
}
class yuanzhu extends li{//圆柱类
	double r,h;
	yuanzhu(double r,double h){
		this.r=r;
		this.h=h;
	}
	public double biao(){//表面积
		return(3.14*r*r+2*3.14*r*h);
	}
	public double ti(){//体积
		return(3.14*r*r*h);
	}
}
class yuanzhui extends li{ //圆锥类
	double r,h,l;
	yuanzhui(double r,double h,double l){
		this.r=r;
		this.h=h;
		this.l=l;
	}
	public double ti(){//体积
		return (1/3*3.14*r*r*h);
	}
	public double biao(){//表面积
		return (3.14*r*l+3.14*r*r);
	}
}
 class juxing extends ping{//矩形
	double c,k;
	juxing(double c,double k){
		this.c=c;
		this.k=k;
	}
	public double mian(){//面积
		return(c*k);
	}
	public double zhou(){
		return(c+c+k+k);
	}
}
class sanjiao extends ping{ //三角
	double d1,h,d2,d3;
	sanjiao(double d1,double d2,double d3 ,double h ){
		this.d1=d1;
		this.d2=d2;
		this.d3=d3;
		this.h=h;
		
	}
	public double zhou(){//周长
		return(d1+d2+d3);
	}
	public double mian(){//面积
		return(d1*h/2);
	}
}
class yuan extends ping{//圆
	double r;
	yuan(double r){
		this.r=r;
	}
	public double zhou(){
		return(2*3.14*r);
	}
	public double mian(){
		return (3.14*2*r*r);
	}
	
}
public class shiyansi3 {
	public static void main(String[] args) {
		int a,c,k;
		Scanner input = new Scanner(System.in);
		double r,x,h,l,d1,d2,d3;
		a=(int)(Math.random()*5);
		if(a==0){
			 r=(int)(Math.random()*5);
			System.out.println("球的半径为"+r+"请计算计算球的表面积");
			x=input.nextDouble();
			qiu b=new qiu(r);
			if(x==b.biao()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.biao());
			}
			System.out.println("请计算计算球的体积");
			x=input.nextDouble();
			if(x==b.ti()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.ti());
			}
		}
		else if(a==1){
			     r=(int)(Math.random()*5);
			     h=(int)(Math.random()*5);
				System.out.println("圆柱的半径为"+r+"高为："+h+"请计算计算圆柱的表面积");
				x=input.nextDouble();
				yuanzhu b=new yuanzhu(r,h);
				if(x==b.biao()) {
					System.out.println("恭喜你回答正确");
				}
				else {
					System.out.println("回答错误,正确的答案为："+b.biao());
				}
			System.out.println("请计算计算圆柱的体积");
			x=input.nextDouble();
			if(x==b.ti()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.ti());
			}
		}
		else if(a==2){
			  r=(int)(Math.random()*5);
			  h=(int)(Math.random()*5);
			  l=(int)(Math.random()*5);
			System.out.println("圆锥的底面半径为："+r+"高为："+h+"母线为："+l+"请计算圆锥的表面积");
			x=input.nextDouble();
			yuanzhui b =new yuanzhui (r,h,l);
			if(x==b.biao()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.biao());
			}
			System.out.println("请计算圆锥的体积");
			x=input.nextDouble();
			if(x==b.ti()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.ti());
			}
		}
		else if(a==3){
			c=(int)(Math.random()*5);
			k=(int)(Math.random()*5);
			System.out.println("矩形的边长为："+c+"\t"+k+"请计算矩形的周长");
			x=input.nextDouble();
			juxing b= new juxing(c,k);
			if(x==b.zhou()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.zhou());
			}
			System.out.println("请计算矩形的面积");
			x=input.nextDouble();
			if(x==b.mian()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.mian());
			}
		}
		else if(a==4){
			  d1=(int)(Math.random()*5);
			  d2=(int)(Math.random()*5);
			  d3=(int)(Math.random()*5);
			  h=(int)(Math.random()*5);
			System.out.println("三角形的三条边分别为："+d1+d2+d3+"请计算三角形的周长");
			x=input.nextDouble();
			sanjiao b= new sanjiao(d1,d2,d3,h);
			if(x==b.zhou()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.zhou());
			}
			System.out.println("请计算三角形的面积");
			x=input.nextDouble();
			if(x==b.mian()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.mian());
			}
		}
		else if(a==5){
			r=(int)(Math.random()*5);
			System.out.println("圆的半径为："+r+"请计算圆的周长");
			x=input.nextDouble();
			yuan b= new yuan(r);
			if(x==b.zhou()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.zhou());
			}
			System.out.println("请计算圆的面积");
			x=input.nextDouble();
			if(x==b.mian()) {
				System.out.println("恭喜你回答正确");
			}
			else {
				System.out.println("回答错误,正确的答案为："+b.mian());
			}
		}
	}
}
